freeslot("MT_EBOMB","MT_EGGEBOMBER")
addHook("ShouldDamage", function(target, inflictor, source, damage, damagetype)
    if inflictor.type == MT_SHOCKWAVE or inflictor.type == MT_FLINGRING or inflictor.type == MT_EBOMB
        return false
    end
end, MT_EGGEBOMBER)